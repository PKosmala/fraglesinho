package permissions.db;

import java.util.List;

import permissions.domain.Address;

public interface AddressRepository extends Repository<Address> {
	
	public List<Address> withStreet(String street, PagingInfo page);
	public List<Address> withCity(String city, PagingInfo page);
	public List<Address> withCountry(String country, PagingInfo page);
	
}
